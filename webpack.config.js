//获取project根路径的依赖库
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const config = {
  //配置入口文件
  entry: "./src/index.js",
  //配置输出文件
  output: {
    //获取project根路径并指定输出文件的目录build
    path: path.resolve(__dirname, "build"),
    //输出文件的名称
    filename: "bundle.js",
    //静态资源输出配置
    publicPath: "build/"
  },
  module: {
    //配置规则
    rules: [
      //es6/7转码规则：对所有js文件采用babel-loader转码
      {
        use: 'babel-loader',
        test: /\.js$/
      },
      //使用如下配置使css打包成单独的文件
      {
        loader: ExtractTextPlugin.extract({
          loader: 'css-loader'
        }),
        test: /\.css$/
      },
      //图片打包处理使用如下配置
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: { limit: 40000 }
          },
          'image-webpack-loader'
        ]
      }
    ]
  },
  plugins: [
    //使用如下配置使css打包成单独的文件style.css
    new ExtractTextPlugin('style.css')
  ]
}

module.exports = config
